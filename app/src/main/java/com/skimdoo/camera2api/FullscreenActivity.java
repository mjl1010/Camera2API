/*
 * Copyright 2018 SKIMDOO
 */

package com.skimdoo.camera2api;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FullscreenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        if (null == savedInstanceState) {
            getSupportActionBar().hide();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_old, Camera2BasicFragment.newInstance())
                    .commit();
        }
    }
}
